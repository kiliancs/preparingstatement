package com.aktive.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Utility class that simplifies the work with PreparedStatements.
 * Constructing queries often involves gluing different parts in a StringBuffer and keeping a
 * counter to set the parameters to pass to the PreparedStatement.
 * The purpose of this class is to handle these for the developer.
 * 
 * Example usages:
 * 
 * PreparingStatement select = new PreparingStatement("select * from employees").appendWhere(PreparingStatement.createWhereMap(colvalues));
 * --> select * from employees where city = 'BCN' and age = 23
 * 
 * PreparingStatement select = new PreparingStatement("select * from employees");
 * PreparingStatement where = new PraparingStatement();
 * while (colvaluesIt.hasNext())
 * {
 * 	Entry<String, Object> colVal = colvaluesIt.next();
 * 	where.appendAnd(String.format("%s = ?", colVal.getKey()), colVal.getValue());
 * }
 * select.appendWhere(where);
 * --> select * from employees where city = 'BCN' and age = 23
 * 
 * PreparingStatement select = new PreparingStatement("select * from employees");
 * PreparingStatement in = new PreparingStatement("city").appendIn(cities);
 * select.appendWhere(in);
 * --> select * from employees where city in ('BCN', 'SEA')
 * 
 * @author Kilian
 *
 */
public class PreparingStatement
{
	private StringBuffer sql;
	private List<PreparedStatementParameter> parameters = new ArrayList<PreparedStatementParameter>();
	
	public List<PreparedStatementParameter> getParameters()
	{
		return parameters;
	}

	public PreparingStatement (String sql)
	{
		this.sql = new StringBuffer(sql);
	}
	
	public PreparingStatement (String sql, Object ... values)
	{
		this.sql = new StringBuffer(sql);
		for (Object o : values)
		{
			this.add(o);
		}
	}
	
	public PreparingStatement (String sql, Collection<?> values)
	{
		this.sql = new StringBuffer(sql);
		for (Object o : values)
		{
			this.add(o);
		}
	}
	
	public PreparingStatement (String sql, Object value, Integer type)
	{
		this.sql = new StringBuffer(sql);
		this.add(value, type);
	}
	
	public PreparingStatement ()
	{
		this.sql = new StringBuffer();
	}
	
	public String getSql ()
	{
		return this.sql.toString();
	}
	
	public String getSql (boolean enclosed)
	{
		return enclosed && sql.length() > 0 ? String.format("(%s)", sql).toString() : sql.toString();
	}
	
	public PreparingStatement addAll(Object ... values)
	{
		for (Object o : values)
		{
			this.add(o);
		}
		return this;
	}
	
	public PreparingStatement addAll(Collection<?> values)
	{
		for (Object o : values)
		{
			this.add(o);
		}
		return this;
	}
	
	public PreparingStatement add(Object value)
	{
		return this.add(value, null);
	}
	
	public PreparingStatement add(Object value, Integer type)
	{
		return this.add(new PreparedStatementParameter(type, value));
	}
	
	public PreparingStatement add(PreparedStatementParameter par)
	{
		this.parameters.add(par);
		return this;
	}
	
	public PreparingStatement add(String object)
	{
		return add(object, java.sql.Types.VARCHAR);
	}
	
	public PreparingStatement add(Integer object)
	{
		return add(object, java.sql.Types.INTEGER);
	}
	
	public PreparingStatement add(Boolean object)
	{
		return add(object, java.sql.Types.BOOLEAN);
	}
	
	public PreparedStatement prepareStatement(Connection con) throws SQLException
	{
		PreparedStatement st = con.prepareStatement(sql.toString());
		Iterator<PreparedStatementParameter> it = parameters.iterator();
		int i = 0;
		while (it.hasNext())
		{
			i++;
			PreparedStatementParameter parameter = it.next();
			Integer type = parameter.getType();
			if (type == null)
			{
				st.setObject(i, parameter.getValue());
			}
			else
			{
				st.setObject(i, parameter.getValue(), parameter.getType());
			}
		}
		return st;
	}
	
	public PreparingStatement append(PreparingStatement part2)
	{
		return this.append(part2, AppendType.APPEND, false);
	}
	
	public PreparingStatement appendAnd(PreparingStatement part2)
	{
		return this.append(part2, AppendType.AND, false);
	}
	
	public PreparingStatement appendOr(PreparingStatement part2)
	{
		return this.append(part2, AppendType.OR, false);
	}
	
	public PreparingStatement appendWhere(PreparingStatement part2)
	{
		return this.append(part2, AppendType.WHERE, false);
	}
	
	public PreparingStatement appendIn(PreparingStatement part2)
	{
		return this.append(part2, AppendType.IN, false);
	}
	
	public PreparingStatement appendIn(Collection<?> valueCollection)
	{
		if (valueCollection.size() == 0)
		{
			return this;
		}
		
		Iterator<?> valIt = valueCollection.iterator();
		PreparingStatement inSt = new PreparingStatement();
		while (valIt.hasNext())
		{
			inSt.appendInValue(valIt.next());
		}
		return this.appendIn(inSt);
	}
	
	public PreparingStatement appendInValue(Object value)
	{
		return this.append(new PreparingStatement("?", value), AppendType.IN_VALUE, false);
	}
	
	public PreparingStatement append(PreparingStatement part2, AppendType appendType, boolean enclose)
	{
		if (part2.isEmpty())
		{
			return this;
		}
		
		boolean encloseThis = appendType == AppendType.IN || appendType == AppendType.IN_VALUE ? false : enclose;
		boolean encloseThat = appendType == AppendType.IN ? true : enclose;
		
		StringBuffer newSql = new StringBuffer(getSql(encloseThis));
		
		if (!isEmpty())
		{
			newSql.append(appendType.toString());
		}
		
		newSql.append(part2.getSql(encloseThat));
		this.sql = newSql;
		
		parameters.addAll(part2.getParameters());
		
		return this;
	}
	
	public PreparingStatement append (String sql)
	{
		this.sql.append(sql);
		return this;
	}
	
	public PreparingStatement append (String sql, PreparedStatementParameter parameter)
	{
		append(sql);
		add(parameter);
		return this;
	}
	
	public PreparingStatement append (String sql, Object value, int type)
	{
		append(sql);
		add(value, type);
		return this;
	}
	
	public PreparingStatement append (String sql, Object value)
	{
		append(sql);
		add(value);
		return this;
	}
	
	public PreparingStatement append (String sql, Object ... values)
	{
		append(sql);
		addAll(values);
		return this;
	}

	public PreparingStatement append (String sql, Collection<?> values)
	{
		append(sql);
		addAll(values);
		return this;
	}
	
	public PreparingStatement append (AppendType appendType, String sql, Object value)
	{
		PreparingStatement ps = new PreparingStatement(sql, value);
		append(ps, appendType, false);
		add(value);
		return this;
	}
	
	public PreparingStatement append (AppendType appendType, String sql, Object ... values)
	{
		PreparingStatement ps = new PreparingStatement(sql, values);
		append(ps, appendType, false);
		addAll(values);
		return this;
	}
	
	public PreparingStatement append (AppendType appendType, String sql, Collection<?> values)
	{
		PreparingStatement ps = new PreparingStatement(sql, values);
		append(ps, appendType, false);
		addAll(values);
		return this;
	}
	
	public PreparingStatement appendAnd (String sql, Object value)
	{
		return append(AppendType.AND, sql, value);
	}
	
	public PreparingStatement appendAnd (String sql, Object ... values)
	{
		return append(AppendType.AND, sql, values);
	}
	
	public PreparingStatement appendAnd (String sql, Collection<?> values)
	{
		return append(AppendType.AND, sql, values);
	}
	
	public PreparingStatement appendOr (String sql, Object value)
	{
		return append(AppendType.OR, sql, value);
	}
	
	public PreparingStatement appendOr (String sql, Object ... values)
	{
		return append(AppendType.OR, sql, values);
	}
	
	public PreparingStatement appendOr (String sql, Collection<?> values)
	{
		return append(AppendType.OR, sql, values);
	}
	
	public PreparingStatement appendWhere (String sql, Object value)
	{
		return append(AppendType.WHERE, sql, value);
	}
	
	public PreparingStatement appendWhere (String sql, Object ... values)
	{
		return append(AppendType.WHERE, sql, values);
	}
	
	public PreparingStatement appendWhere (String sql, Collection<?> values)
	{
		return append(AppendType.WHERE, sql, values);
	}
	
	public static PreparingStatement createWhere (Map<String, ?> keyMap)
	{
		PreparingStatement where = new PreparingStatement();
		Iterator<?> kmIt = keyMap.entrySet().iterator();
		while (kmIt.hasNext())
		{
			@SuppressWarnings("unchecked")
			Entry<String, ?> keyParam = (Entry<String, ?>) kmIt.next();
			
			Object val = keyParam.getValue();
			
			PreparingStatement valst = new PreparingStatement(keyParam.getKey());
			
			if (val == null)
			{
				valst.append(" is null");
			}
			else
			{
				valst.append(" = ?", val);
			}
			
			where.appendAnd(valst);
		}
		return where;
	}
	
	public static PreparingStatement createSet (Map<String, ?> keyMap)
	{
		PreparingStatement where = new PreparingStatement();
		Iterator<?> kmIt = keyMap.entrySet().iterator();
		while (kmIt.hasNext())
		{
			@SuppressWarnings("unchecked")
			Entry<String, ?> keyParam = (Entry<String, ?>) kmIt.next();
			
			where.append(keyParam.getKey());
			
			Object val = keyParam.getValue();
			if (val == null)
			{
				where.append(" = null");
			}
			else
			{
				where.append(" = ?", val);
			}
		}
		return where;
	}
	
	private static Pattern namedParameterPattern = Pattern.compile(":([a-z0-9_]+)", Pattern.CASE_INSENSITIVE);
	public static PreparingStatement createNamed (String sql, Map<String, Object> keyMap)
	{
		PreparingStatement ps = new PreparingStatement();
		Matcher m = namedParameterPattern.matcher(sql);
		StringBuffer newSql = new StringBuffer();
		while (m.find())
		{
			String varName = m.group(1);
			if (!keyMap.containsKey(varName))
			{
				continue;
			}
			ps.add(keyMap.get(varName));
			m.appendReplacement(newSql, Matcher.quoteReplacement("?"));
		}
		m.appendTail(newSql);
		return ps.append(newSql.toString());
	}
	
	public static String[] extractNamedParameters(String sql)
	{
		Set<String> parameters = new HashSet<String>();
		Matcher m = namedParameterPattern.matcher(sql);
		while (m.find())
		{
			String varName = m.group(1);
			parameters.add(varName);
		}
		return parameters.toArray(new String[parameters.size()]);
	}
	
	public static PreparingStatement createSelect(String ... parameters)
	{
		PreparingStatement ps = new PreparingStatement();
		for (String param : parameters)
		{
			ps.append(new PreparingStatement(param), AppendType.COMMA, false);
		}
		return ps;
	}
	
	public boolean isEmpty()
	{
		return sql.length() == 0;
	}
	
	@Override
	public String toString()
	{
		StringBuffer strRep = new StringBuffer(this.sql).append(" {");
		Iterator<PreparedStatementParameter> pit = this.parameters.iterator();
		int i = 0;
		while (pit.hasNext())
		{
			PreparedStatementParameter par = pit.next();
			
			String valRep;
			if (par.getValue() == null)
			{
				valRep = "null";
			}
			else if (par.getValue() instanceof String)
			{
				valRep = String.format("\"%s\"", par.getValue());
			}
			else
			{
				valRep = par.getValue().toString();
			}
			i++;
			strRep.append(String.format(" #%d: %s,", i, valRep));
		}
		strRep.append(" }");
		return strRep.toString();
	}
	
	public enum AppendType
	{
		APPEND, AND, OR, IN, IN_VALUE, WHERE, COMMA;
		
		public String toString()
		{
			switch (this)
			{
				case APPEND: return " ";
				case AND: return " and ";
				case OR: return " or ";
				case IN: return " in ";
				case IN_VALUE:
				case COMMA: return ", ";
				case WHERE: return " where ";
				default: throw new IllegalArgumentException();
			}
		}
	}
	
	public class PreparedStatementParameter
	{
		private Integer type;
		private Object value;
		
		public PreparedStatementParameter (Integer type, Object value)
		{
			this.type = type;
			this.value = value;
		}
		
		public PreparedStatementParameter (Object value)
		{
			this.value = value;
		}
		
		public Integer getType()
		{
			return type;
		}

		public Object getValue()
		{
			return value;
		}
	}
}
